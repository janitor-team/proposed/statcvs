We are pleased to announce the StatCVS 0.6.0 release! http://statcvs.sf.net 

StatCVS retrieves information from a CVS repository and generates various tables and charts describing the project development, 
e.g. timeline for the lines of code, contribution of each developer etc. The current version of StatCVS generates a static 
suite of HTML documents containing tables and chart images. StatCVS is open source software, released under the terms of 
the LGPL. StatCVS uses JFreeChart and JTreeMap to generate charts. 

=============================================================================== 

Changes in this version include: 

New Features: 

** Inspired by StatCVS-xml, A module mechanism is added to StatCVS whereby it is now possible to define
   some regular expression to group files into modules and have a dedicated set of statistics per module
** 2 new command line options: headerUrl and footerUrl the content of which will 
   be incorporated just after opening body or just before the end of body.
*  The latest commit page will always be called commitlog.html Thanks to David Multer

* fix 	Fixing NPE issue and bad link when there are no commits in a repository. Thanks to David Multer


=============================================================================== 
Issues, bugs, and feature requests for statcvs should be submitted to the following issue tracking 
system: http://sourceforge.net/tracker/?group_id=57558 

Have fun! 

-The StatCVS development team and http://www.appendium.com 

